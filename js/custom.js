
// Read More (About)

var open=false;


$('#span').click (function() {

    $('.read-more').slideToggle();
    if (open===true){
        $(this).text('Read More');
        open=false;
   
    } else {

        $(this).text('Show Less');
        open=true;
    }
});



// Contact Details

$('.contact-btn').click (function() {

    $('.contact-details').slideDown();

});


// Burger nav


document.querySelector('.burger').addEventListener('click', function() {

    document.querySelector('.header nav').classList.toggle('open');
    document.querySelector('.burger').classList.toggle('open');

} );


$('nav a').click(function(){
  console.log('nav click');
  $('.header nav').removeClass('open');
  $('.burger').removeClass('open');
});




// Testimonials 

// $('.carousel').slick({
//     dots: true,
//     infinite: true,
//     speed: 600,
//     // fade: true,
//     cssEase: 'linear'
// });  


  $('.carousel').slick({
    dots:true,
    autoplay: false,
    slidesToShow:1,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="slick-prev">Previous</button>',
    nextArrow: '<button type="button" class="slick-next">Next</button>',
    responsive: [
      {
        // breakpoint: 1024,
        settings: { slidesToShow: 1 }
      },{
        // breakpoint: 600,
        settings: { slidesToShow: 1 }
      },{
        // breakpoint: 480,
        settings: { slidesToShow: 1 }
      }
     ]
  });


// Scroll Plant

var plantsRunOnce = false;
$(window).on('scroll', function () {  
  if( $('svg').offset().top < ( $(window).scrollTop() + ($(window).height()) ) ){
    if(plantsRunOnce === false) {
      generate();
      plantsRunOnce = true;
    }
  }
});


// Scroll from CodePen example

  // $(window).on('scroll', function () {
  //   if( $('.planter').offset().top < ( $(window).scrollTop() + ($(window).height(2)) ) ){
  //     // Fires when element is in view
  //     $('.planter').addClass('.visible'); // Change action
  //   }else{
  //     // Removes when element is out of view
  //     $('.planter').removeClass('.visible');// Change action
  //   }
  // });